package dk.semlerit.autocore.infrastructure.filter;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import com.ibm.portal.auth.ExplicitLoginFilter;
import com.ibm.portal.auth.ExplicitLoginFilterChain;
import com.ibm.portal.auth.FilterChainContext;
import com.ibm.portal.auth.exceptions.AuthenticationException;
import com.ibm.portal.auth.exceptions.AuthenticationFailedException;
import com.ibm.portal.auth.exceptions.LoginException;
import com.ibm.portal.auth.exceptions.PasswordInvalidException;
import com.ibm.portal.auth.exceptions.SystemLoginException;
import com.ibm.portal.auth.exceptions.UserIDInvalidException;
import com.ibm.portal.security.SecurityFilterConfig;
import com.ibm.portal.security.exceptions.SecurityFilterInitException;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;

/**
 * Customized explicit login filter to ensure that standard users are
 * re-directed to the AutoCore Portal v9 Theme.
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class CustomExplicitLoginFilter implements ExplicitLoginFilter {
	private static final String CLASS_NAME = CustomExplicitLoginFilter.class.getName();
	private static final Logger log = Logger.getLogger(CLASS_NAME);
	private static final String LDAP_URI_PARAM = "ldapUri";
	private static final String DEFAULT_LDAP_URI = "acplddt01.semlernet.dk";

	private static final String LDAP_PORT_PARAM = "ldapPort";
	private static final int DEFAULT_LDAP_PORT = 389;

	private static final String USER_HOME_PARAM = "userHome";
	private static final String DEFAULT_PORTAL_HOME = "/wps/myportal";

	private String userHome = DEFAULT_PORTAL_HOME;
	private String ldapURI = DEFAULT_LDAP_URI;
	private int ldapPort = DEFAULT_LDAP_PORT;

	@Override
	public void login(HttpServletRequest request, HttpServletResponse response, String userID, char[] password,
			FilterChainContext portalLoginContext, Subject subject, String realm, ExplicitLoginFilterChain chain)
			throws javax.security.auth.login.LoginException, com.ibm.websphere.security.WSSecurityException,
			PasswordInvalidException, UserIDInvalidException, AuthenticationFailedException, AuthenticationException,
			SystemLoginException, LoginException {

		chain.login(request, response, userID, password, portalLoginContext, subject, realm);

		if (isAdmin(userID)) {
			/*
			 * Administrators should not get redirected to AutoCore Portal Theme
			 * v9
			 */
			return;
		}

		/* Set non-admin home. */
		portalLoginContext.setRedirectURL(userHome);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void init(SecurityFilterConfig securityFilterConfig) throws SecurityFilterInitException {
		securityFilterConfig.getInitParameterNames().forEachRemaining(p -> {
			switch ((String) p) {
			case USER_HOME_PARAM:
				this.userHome = Optional.ofNullable(securityFilterConfig.getInitParameter((String) p))
						.orElse(DEFAULT_PORTAL_HOME);
				break;
			case LDAP_URI_PARAM:
				this.ldapURI = Optional.ofNullable(securityFilterConfig.getInitParameter((String) p))
						.orElse(DEFAULT_LDAP_URI);
				break;
			case LDAP_PORT_PARAM:
				this.ldapPort = Optional.ofNullable(tryParseInt(securityFilterConfig.getInitParameter((String) p)))
						.orElse(DEFAULT_LDAP_PORT);
				break;
			default:
				log.info(String.format("Unknown config param found: %s", (String) p));
				/* Ignore unknown params */
			}
		});

		log.info("Custom Explicit Login filter was successfully initialized.");
	}

	@Override
	public void destroy() {
	}

	private Integer tryParseInt(String str) {
		try {
			return Integer.parseInt(str);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	private boolean isAdmin(@NotNull String userID) {
		try (LDAPConnection conn = new LDAPConnection(ldapURI, ldapPort, "cn=ACPBind,o=SMC", "semadmin")) {
			Objects.requireNonNull(userID, "userID must not be null");
			final String dn = conn.search(new SearchRequest("o=SMC", SearchScope.SUB,
					Filter.createORFilter(Arrays.asList(new Filter[] { Filter.create(String.format("(cn=%s)", userID)),
							Filter.create(String.format("(mail=%s)", userID)) }))))
					.getSearchEntries().stream().findFirst().orElseThrow(() -> new IllegalArgumentException(
							String.format("No DistinguishedName could be found for identifier: %s", userID)))
					.getDN();

			final SearchResultEntry entry = conn
					.search(new SearchRequest("", SearchScope.SUB, Filter.createANDFilter(Arrays.asList(new Filter[] {
							Filter.create("(objectClass=groupOfNames)"), Filter.create("(cn=acp.websphere.admin)"),
							Filter.create(String.format("(member=%s)", dn)) }))))
					.getSearchEntry("cn=acp.websphere.admin");

			return Objects.nonNull(entry);
		} catch (LDAPException e) {
			log.logp(Level.SEVERE, CLASS_NAME, "isAdmin",
					String.format("Could not determine administrative rights for userID: %s", userID), e);
			return false;
		}
	}
}
