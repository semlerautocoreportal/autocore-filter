/**
 * Semler It - Autocore Portal
 */
package dk.semlerit.autocore.infrastructure.filter;

import java.io.IOException;
import java.util.Objects;

import javax.naming.CompositeName;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;

import com.ibm.portal.ModelException;
import com.ibm.portal.content.ContentNode;
import com.ibm.portal.identification.Identification;
import com.ibm.portal.model.NavigationSelectionModelHome;
import com.ibm.portal.model.NavigationSelectionModelProvider;
import com.ibm.portal.navigation.NavigationNode;
import com.ibm.portal.navigation.NavigationSelectionModel;
import com.ibm.portal.serialize.SerializationException;
import com.ibm.portal.um.PumaHome;
import com.ibm.websphere.cache.DistributedMap;

import dk.semlerit.autocore.infrastructure.filter.core.OncePerRequestFilter;
import dk.semlerit.autocore.infrastructure.filter.core.RequestUtils;
import dk.semlerit.autocore.web.core.context.CoreConstants;
import dk.semlerit.autocore.web.core.context.UserContextAccessor;
import dk.semlerit.autocore.web.core.puma.DefaultPumaHomeSupport;
import dk.semlerit.autocore.web.core.puma.PumaHomeSupport;

/**
 * 
 *
 * @author admbrpe
 * @version 1.0
 * @since 1.0.0
 */
public class DealerNumberResetFilter extends OncePerRequestFilter {
	private static final String USER_CONTEXT_JNDINAME = "services/cache/autocore/acusercontext";
	private NavigationSelectionModelProvider navigationSelectionModelProvider;
	private Identification identification;
	private UserContextAccessor userContextAccessor;
	private PumaHomeSupport pumaHomeSupport;

	@Override
	public void init(FilterConfig filterConfig) throws PortletException {
		super.init(filterConfig);
		try {
			DistributedMap userContextMap = Objects.requireNonNull(
					InitialContext.doLookup(new CompositeName(USER_CONTEXT_JNDINAME)),
					String.format("Could not lookup: %s", USER_CONTEXT_JNDINAME));
			PumaHome pumaHome = Objects.requireNonNull(InitialContext.doLookup(new CompositeName(PumaHome.JNDI_NAME)),
					String.format("Could not lookup: %s", PumaHome.JNDI_NAME));
			pumaHomeSupport = new DefaultPumaHomeSupport();
			((DefaultPumaHomeSupport) pumaHomeSupport).setPumaHome(pumaHome);

			userContextAccessor = new UserContextAccessor();
			userContextAccessor.setUserContextMap(userContextMap);
			userContextAccessor.setPumaHome(pumaHome);
		} catch (NullPointerException e) {
			throw new PortletException(e.getMessage(), e);
		} catch (NamingException e) {
			throw new PortletException(e);
		}

		try {
			navigationSelectionModelProvider = ((NavigationSelectionModelHome) InitialContext
					.doLookup(new CompositeName(NavigationSelectionModelHome.JNDI_NAME)))
							.getNavigationSelectionModelProvider();
			identification = InitialContext.doLookup(new CompositeName(Identification.JNDI_NAME));
		} catch (NamingException e) {
			throw new PortletException(e);
		}
	}

	@Override
	protected void doFilterInternal(ActionRequest request, ActionResponse response, FilterChain filterChain)
			throws PortletException, IOException {

		boolean hasChanged = userContextAccessor.hasDealerNumberChanged(request, CoreConstants.DEFAULT_SESSION_ALIAS);
		if (hasChanged) {
			request.setAttribute("DN_RESET_REQUIRED", request.getWindowID());
		}

		filterChain.doFilter(request, response);

		if (hasChanged) {
			request.removeAttribute("DN_RESET_REQUIRED");
		}
	}

	@Override
	protected void doFilterInternal(RenderRequest request, RenderResponse response, FilterChain filterChain)
			throws PortletException, IOException {
		boolean hasChanged = userContextAccessor.hasDealerNumberChanged(request, CoreConstants.DEFAULT_SESSION_ALIAS);
		if (hasChanged) {
			request.setAttribute("DN_RESET_REQUIRED", request.getWindowID());
		}

		filterChain.doFilter(request, response);

		if (hasChanged) {
			request.removeAttribute("DN_RESET_REQUIRED");
		}

		userContextAccessor.setLastPageVisited(request, CoreConstants.DEFAULT_SESSION_ALIAS,
				getPageObjectID(request, response));
	}

	@Override
	protected void doFilterInternal(ResourceRequest request, ResourceResponse response, FilterChain filterChain)
			throws PortletException, IOException {

		boolean hasChanged = userContextAccessor.hasDealerNumberChanged(request, CoreConstants.DEFAULT_SESSION_ALIAS);
		if (hasChanged) {
			request.setAttribute("DN_RESET_REQUIRED", request.getWindowID());
		}

		filterChain.doFilter(request, response);

		if (hasChanged) {
			request.removeAttribute("DN_RESET_REQUIRED");
		}
	}

	@Override
	protected void doFilterInternal(EventRequest request, EventResponse response, FilterChain filterChain)
			throws PortletException, IOException {
		boolean hasChanged = userContextAccessor.hasDealerNumberChanged(request, CoreConstants.DEFAULT_SESSION_ALIAS);
		if (hasChanged) {
			request.setAttribute("DN_RESET_REQUIRED", request.getWindowID());
		}

		filterChain.doFilter(request, response);

		if (hasChanged) {
			request.removeAttribute("DN_RESET_REQUIRED");
		}

	}

	// ~ Private internal methods ==============================================

	private String getPageObjectID(PortletRequest request, PortletResponse response) throws PortletException {
		try {
			NavigationSelectionModel<?> navigationSelectionModel = navigationSelectionModelProvider
					.getNavigationSelectionModel(RequestUtils.getHttpServletRequest(request),
							RequestUtils.getHttpServletResponse(request, response));
			NavigationNode navigationNode = navigationSelectionModel.getSelectedNode();
			ContentNode contentNode = navigationNode.getContentNode();

			return identification.serialize(contentNode.getObjectID());
		} catch (IllegalStateException | SerializationException | ModelException e) {
			// We are not within sane portal scope.. Ignore...
			return "N/A";
		}
	}
}
