package dk.semlerit.autocore.infrastructure.filter;

import java.io.IOException;
import java.util.UUID;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.filter.FilterChain;

import org.slf4j.MDC;

import dk.semlerit.autocore.infrastructure.filter.core.OncePerRequestFilter;

/**
 * MDC Context filter which provides additional values to the Log4j2 context
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 1.0
 */
public class MDCContextFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(final ActionRequest request, final ActionResponse response,
			final FilterChain filterChain) throws PortletException, IOException {
		try {
			MDC.put("CorrelationID", UUID.randomUUID().toString());
			MDC.put("UserID", request.getRemoteUser());

			filterChain.doFilter(request, response);

		} finally {
			MDC.clear();
		}
	}

	@Override
	protected void doFilterInternal(final RenderRequest request, final RenderResponse response,
			final FilterChain filterChain) throws PortletException, IOException {
		try {
			MDC.put("CorrelationID", UUID.randomUUID().toString());
			MDC.put("UserID", request.getRemoteUser());

			filterChain.doFilter(request, response);
		} finally {
			MDC.clear();
		}
	}

	@Override
	protected void doFilterInternal(final ResourceRequest request, final ResourceResponse response,
			final FilterChain filterChain) throws PortletException, IOException {
		try {
			MDC.put("CorrelationID", UUID.randomUUID().toString());
			MDC.put("UserID", request.getRemoteUser());

			filterChain.doFilter(request, response);
		} finally {
			MDC.clear();
		}
	}

	@Override
	protected void doFilterInternal(final EventRequest request, final EventResponse response,
			final FilterChain filterChain) throws PortletException, IOException {
		try {
			MDC.put("CorrelationID", UUID.randomUUID().toString());
			MDC.put("UserID", request.getRemoteUser());

			filterChain.doFilter(request, response);
		} finally {
			MDC.clear();
		}
	}
}
