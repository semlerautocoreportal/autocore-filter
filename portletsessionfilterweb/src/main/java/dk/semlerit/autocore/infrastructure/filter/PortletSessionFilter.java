/**
 * Semler It - Autocore Portal
 */
package dk.semlerit.autocore.infrastructure.filter;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.filter.FilterChain;

import dk.semlerit.autocore.infrastructure.filter.core.OncePerRequestFilter;

/**
 * Ensure as early as possible that every portlet has a valid session. If portletsession does not exist, it is created
 */
public class PortletSessionFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(ActionRequest request, ActionResponse response, FilterChain filterChain)
			throws PortletException, IOException {
		PortletSession s = request.getPortletSession();
	}

	@Override
	protected void doFilterInternal(RenderRequest request, RenderResponse response, FilterChain filterChain)
			throws PortletException, IOException {
		PortletSession s = request.getPortletSession();
	}

	@Override
	protected void doFilterInternal(ResourceRequest request, ResourceResponse response, FilterChain filterChain)
			throws PortletException, IOException {
		PortletSession s = request.getPortletSession();
	}

	@Override
	protected void doFilterInternal(EventRequest request, EventResponse response, FilterChain filterChain)
			throws PortletException, IOException {
		PortletSession s = request.getPortletSession();
	}
}