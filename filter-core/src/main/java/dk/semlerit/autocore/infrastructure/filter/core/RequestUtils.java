/**
 * Semler It - Autocore Portal
 */
package dk.semlerit.autocore.infrastructure.filter.core;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import com.ibm.ws.portletcontainer.portlet.PortletUtils;

/**
 * 
 *
 * @author admbrpe
 * @version 1.0
 * @since 1.0.0
 */
public class RequestUtils {

	/**
	 * 
	 * @param request
	 * @return
	 */
	public static HttpServletRequest getHttpServletRequest(final PortletRequest request) {
		HttpServletRequest httpServletRequest = PortletUtils.createIncludedServletRequest(request);
		while (httpServletRequest instanceof HttpServletRequestWrapper) {
			HttpServletRequestWrapper httpServletRequestWrapper = (HttpServletRequestWrapper) httpServletRequest;
			httpServletRequest = (HttpServletRequest) httpServletRequestWrapper.getRequest();
		}
		return httpServletRequest;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public static HttpServletResponse getHttpServletResponse(PortletRequest request, PortletResponse response) {
		HttpServletResponse httpServletResponse = PortletUtils.createIncludedServletResponse(request, response);
		while (httpServletResponse instanceof HttpServletResponseWrapper) {
			HttpServletResponseWrapper httpServletResponseWrapper = (HttpServletResponseWrapper) httpServletResponse;
			httpServletResponse = (HttpServletResponse) httpServletResponseWrapper.getResponse();
		}
		return httpServletResponse;
	}

	private RequestUtils() {
	}
}
