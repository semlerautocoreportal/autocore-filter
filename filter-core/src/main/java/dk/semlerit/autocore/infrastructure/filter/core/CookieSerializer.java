package dk.semlerit.autocore.infrastructure.filter.core;

import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 1.0
 */
public interface CookieSerializer {

	/**
	 * Writes a given {@link CookieValue} to the provided PortletResponse.
	 * 
	 * @param cookieValue
	 *            the {@link CookieValue} to write to
	 *            {@link CookieValue#getResponse()}. Cannot be null.
	 */
	void writeCookieValue(CookieValue cookieValue);

	/**
	 * Reads all the matching cookies from the {@link PortletRequest}. The
	 * result is a List since there can be multiple {@link Cookie} in a single
	 * request with a matching name. For example, one Cookie may have a path of
	 * / and another of /context, but the path is not transmitted in the
	 * request.
	 * 
	 * @param request
	 *            the {@link HttpServletRequest} to read the cookie from. Cannot
	 *            be null.
	 * @return the values of all the matching cookies
	 */
	List<String> readCookieValues(PortletRequest request);

	class CookieValue {
		private final PortletRequest request;
		private final PortletResponse response;
		private final String cookieValue;

		/**
		 * Creates a new instance.
		 * 
		 * @param request
		 *            the {@link HttpServletRequest} to use. Useful for
		 *            determining the context in which the cookie is set. Cannot
		 *            be null.
		 * @param response
		 *            the {@link HttpServletResponse} to use.
		 * @param cookieValue
		 *            the value of the cookie to be written. This value may be
		 *            modified by the {@link CookieSerializer} when writing to
		 *            the actual cookie so long as the original value is
		 *            returned when the cookie is read.
		 */
		public CookieValue(PortletRequest request, PortletResponse response, String cookieValue) {
			this.request = request;
			this.response = response;
			this.cookieValue = cookieValue;
		}

		/**
		 * Gets the request to use.
		 * 
		 * @return the request to use. Cannot be null.
		 */
		public PortletRequest getRequest() {
			return this.request;
		}

		/**
		 * Gets the response to write to.
		 * 
		 * @return the response to write to. Cannot be null.
		 */
		public PortletResponse getResponse() {
			return this.response;
		}

		/**
		 * The value to be written. This value may be modified by the
		 * {@link CookieSerializer} before written to the cookie. However, the
		 * value must be the same as the original when it is read back in.
		 * 
		 * @return the value to be written
		 */
		public String getCookieValue() {
			return this.cookieValue;
		}
	}
}