package dk.semlerit.autocore.infrastructure.filter.core;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.EventFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.RenderFilter;
import javax.portlet.filter.ResourceFilter;
import javax.servlet.ServletException;

/**
 * Allows for easily ensuring that a filter is only invoked once per request. This is a simplified adapted version of
 * spring-web's OncePerRequestFilter and copied to reduce the foot print required to use the session support.
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 1.0
 */
public abstract class OncePerRequestFilter implements RenderFilter, ActionFilter, ResourceFilter, EventFilter {
	public static final String ALREADY_FILTERED_SUFFIX = ".FILTERED";
	private final String alreadyFilteredAttributeName = getClass().getName().concat(ALREADY_FILTERED_SUFFIX);

	@Override
	public void doFilter(EventRequest eventRequest, EventResponse eventResponse, FilterChain filterChain)
			throws IOException, PortletException {

		if (hasAlreadyBeenFiltered(eventRequest)) {
			filterChain.doFilter(eventRequest, eventResponse);
		} else {
			eventRequest.setAttribute(this.alreadyFilteredAttributeName, Boolean.TRUE);
			try {
				doFilterInternal(eventRequest, eventResponse, filterChain);
			} finally {
				eventRequest.removeAttribute(this.alreadyFilteredAttributeName);
			}
		}
	}

	@Override
	public void doFilter(ResourceRequest resourceRequest, ResourceResponse resourceResponse, FilterChain filterChain)
			throws IOException, PortletException {

		if (hasAlreadyBeenFiltered(resourceRequest)) {
			filterChain.doFilter(resourceRequest, resourceResponse);
		} else {
			resourceRequest.setAttribute(this.alreadyFilteredAttributeName, Boolean.TRUE);
			try {
				doFilterInternal(resourceRequest, resourceResponse, filterChain);
			} finally {
				resourceRequest.removeAttribute(this.alreadyFilteredAttributeName);
			}
		}
	}

	@Override
	public void doFilter(RenderRequest renderRequest, RenderResponse renderResponse, FilterChain filterChain)
			throws IOException, PortletException {

		if (hasAlreadyBeenFiltered(renderRequest)) {
			filterChain.doFilter(renderRequest, renderResponse);
		} else {
			renderRequest.setAttribute(this.alreadyFilteredAttributeName, Boolean.TRUE);
			try {
				doFilterInternal(renderRequest, renderResponse, filterChain);
			} finally {
				renderRequest.removeAttribute(this.alreadyFilteredAttributeName);
			}
		}
	}

	@Override
	public void doFilter(ActionRequest actionRequest, ActionResponse actionResponse, FilterChain filterChain)
			throws IOException, PortletException {

		if (hasAlreadyBeenFiltered(actionRequest)) {
			filterChain.doFilter(actionRequest, actionResponse);
		} else {
			actionRequest.setAttribute(this.alreadyFilteredAttributeName, Boolean.TRUE);
			try {
				doFilterInternal(actionRequest, actionResponse, filterChain);
			} finally {
				actionRequest.removeAttribute(this.alreadyFilteredAttributeName);
			}
		}
	}

	/**
	 * Same contract as for {@code ActionFilter#doFilter(ActionRequest, ActionResponse, FilterChain)} , but guaranteed
	 * to be just invoked once per request within a single request thread.
	 * <p>
	 * Provides HttpServletRequest and HttpServletResponse arguments instead of the default ServletRequest and
	 * ServletResponse ones.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param filterChain
	 *            the FilterChain
	 * @throws ServletException
	 *             thrown when a non-I/O exception has occurred
	 * @throws IOException
	 *             thrown when an I/O exception of some sort has occurred
	 */
	protected abstract void doFilterInternal(ActionRequest request, ActionResponse response, FilterChain filterChain)
			throws PortletException, IOException;

	/**
	 * Same contract as for {@code RenderFilter#doFilter(RenderRequest, RenderResponse, FilterChain)} , but guaranteed
	 * to be just invoked once per request within a single request thread.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param filterChain
	 *            the FilterChain
	 * @throws PortletException
	 *             thrown when a non-I/O exception has occurred
	 * @throws IOException
	 *             thrown when an I/O exception of some sort has occurred
	 */
	protected abstract void doFilterInternal(RenderRequest request, RenderResponse response, FilterChain filterChain)
			throws PortletException, IOException;

	/**
	 * Same contract as for {@code ResourceFilter#doFilter(ResourceRequest, ResourceResponse, FilterChain)} , but
	 * guaranteed to be just invoked once per request within a single request thread.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param filterChain
	 *            the FilterChain
	 * @throws PortletException
	 *             thrown when a non-I/O exception has occurred
	 * @throws IOException
	 *             thrown when an I/O exception of some sort has occurred
	 */
	protected abstract void doFilterInternal(ResourceRequest request, ResourceResponse response,
			FilterChain filterChain) throws PortletException, IOException;

	/**
	 * Same contract as for {@code EventFilter#doFilter(EventRequest, EventResponse, FilterChain)}, but guaranteed to be
	 * just invoked once per request within a single request thread.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param filterChain
	 *            the FilterChain
	 * @throws PortletException
	 *             thrown when a non-I/O exception has occurred
	 * @throws IOException
	 *             thrown when an I/O exception of some sort has occurred
	 */
	protected abstract void doFilterInternal(EventRequest request, EventResponse response, FilterChain filterChain)
			throws PortletException, IOException;

	// ~ Interface contract methods ============================================

	@Override
	public void init(FilterConfig filterConfig) throws PortletException {
	}

	@Override
	public void destroy() {
	}

	// ~ Private methods =======================================================

	private boolean hasAlreadyBeenFiltered(final PortletRequest portletRequest) {
		return null != portletRequest.getAttribute(this.alreadyFilteredAttributeName);
	}
}